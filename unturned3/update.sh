#!/bin/bash
# This script installs / updates steamcmd and Unturned 3 on Linux machines
# Syntax: update.sh
# Author: fr34kyn01535 Howdy

STEAMCMD_HOME="$PWD/../steamcmd"
UNTURNED_HOME="$PWD"

cd $STEAMCMD_HOME
./steamcmd.sh +login anonymous +force_install_dir $UNTURNED_HOME +app_update 1110390 validate +exit
