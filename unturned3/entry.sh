#!/bin/sh

./update.sh

cp linux64/steamclient.so steamclient.so

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`/unturned/Unturned_Headless_Data/Plugins/x86_64/`
export LANG=C

DIR_PATH="Servers/$UNTURNED_SERVER/Server"
FILE_PATH="$DIR_PATH/Commands.dat"
mkdir -p $DIR_PATH
touch $FILE_PATH
sed -i -E '/port +[0-9]+/d' $FILE_PATH
echo "port $PORT_START" >> $FILE_PATH

./Unturned_Headless.x86_64 -batchmode -nographics +secureserver/$UNTURNED_SERVER
